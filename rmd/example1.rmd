---
title: "Example 1 Title"
author: "author"
published: "2015-05-17"
updated: ""
---

# Example 1 Title

## This is a Level 2 Heading

Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.

### This is a Level 3 Heading

No vim affert ignota malorum, causae euismod feugait cu quo. Cu nec torquatos reformidans, ei nam petentium prodesset quaerendum. Ius prompta appareat expetenda no, in eros invidunt scribentur sea. At viris maiorum vim, at vel prima fuisset menandri. Vel ne utinam dictas.

#### This is a Level 4 Heading

Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.

## Here is a Second Level 2 Heading

At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.

## Now we run R code

You can embed an R code chunk like this:

```{r}
summary(cars)
```

You can also embed plots, for example:

```{r, echo=FALSE}
plot(cars)
```
